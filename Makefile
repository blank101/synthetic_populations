
RAWDATADIR := raw_population_data
RAWDATAURL := http://www.beta.inegi.org.mx/contenidos/masiva/denue/denue_31_csv.zip

# these are shorthand targets, so the user doesn't need to memorize / type
# ridiculous file names
.PHONY: download

download: $(PTHTODATA)

$(RAWDATADIR):
	mkdir $@

PTHTODATA := $(RAWDATADIR)/conjunto_de_datos/denue_inegi_31_.csv

src.zip:
	wget $(RAWDATAURL) -O $@

$(PTHTODATA): src.zip | $(RAWDATADIR)
	unzip $< -d $(RAWDATADIR)

################################################################################
# elements that need heavy customization relative to raw data + desired input
# to simulation

# workplaces schema:
# longitude, latitude, # of employees
workplaces.csv:
	touch $@

# households schema:
# longitude, latitude, ??? need to represent ages, genders as well
# maybe this is multi-part?  like a household composition file as well?
households.csv:
	touch $@

# schools schema
# longitude, latitude, ? occupancy?
schools.csv:
	touch $@

################################################################################
# elements that may need some customization, mostly on what's needed in
# simulation

# locations.csv schema:
# id, metadata...
# for Yucatan simulation: id, category in [household, school, work], longitude, latitude
locations.csv: workplaces.csv households.csv schools.csv

# people.csv schema:
# id, metadata..., location data...
# in the Yucatan model, metadata is gender, age;
# location data is household location id, then "work" location id (which can be a workplace, school, or the same household)
people.csv: households.csv workplaces.csv schools.csv

# demo_transitions.csv schema:
# person id, person id
# in the Yucatan model, we move around immunity profiles
demo_transitions.csv: people.csv households.csv

# mosquito_movements.csv schema:
# there are movement probabilities between location ids, with most being zero
# and the non-zero ones not necessarily being symmetric # TRUE?
# i.e., the movement probability matrix is sparse

mosquito_movements.csv: locations.csv

synthetic_pop.sqlite: locations.csv people.csv demo_transitions.csv mosquito_movements.csv