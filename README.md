# OVERVIEW

This project includes several generic scripting tools for getting raw data into the format required by the dengue simulation model behind the publications Hladish et al. 2016 and 2018.

Within that simulation model, individuals and mosquitoes can move between locations.  All individuals have a household location, and based on their age, individuals may move from that household to either a school or workplace (or not move at all).  Mosquitoes probabilistic move between "close" locations of any type, based on a weighted adjacency matrix for those locations.

So within the model, the meaning of "space" is simplified to (1) the locations people oscillate between and (2) the relocation probability for mosquitoes.  I.e., we have abstracted away actual physical geography.  However, for our representation of the State of Yucatan, the particular values for both of those meanings of "space" are derived from latitude-longitude coordinate values and geodesic distances between them.  

We do not have the lat-long of each household or workplace in the State of Yucatan.  Instead, we have...  Using that data, we locate households by... We locate workplaces by...

For schools, we have a list of addresses from the Mexican Department of Education, but those are not lat-long coordinates.

People that work have those workplaces set in the model world by a gravity model, which uses geodesic distance between 

# MODEL INPUTS

The model needs four inputs that are related to "place":

1. the location metadata, a list of sequential data concerning places.  In our simulator, we consider three pieces of metadata (latitude, longitude, and category) and since our data consists of relatively few rows and fields (order 100k rows, 3 data fields + an id), we use csv format.  Generically a model might use more than just those three fields, and might have enough rows (or other engineering constraint) to want some other input format (e.g., SQL database).

2. the people metadata, including place association data, a list of sequential data describing people and connecting them to places.  Our simulator has people move between their homes and a daily occupation.

3. mosquito movement probabilities

4. demographic transition data

# SCHOOLS
